import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        "primary_gray" : "#2B2B2B",
        "dark_gray": "#333333",
        "green": "#009379",
        "blue": "#3366FF",
        "red": "#FF6250",
        "yellow": "#FFDF90",
        "purple": "#BFAFF2",
        "light_gray": "#C4C4C4",
        "light_yellow": "#FDF5DF",
      }
    },
  },
  plugins: [],
}
export default config
