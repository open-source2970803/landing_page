"use client"

import Link from "next/link"

export default function Footer() {
    return (
        <div className="mx-[2.5rem] sm:mx-[5rem] py-[3.3rem] border-t border-white/20">
            <h2 className="text-[2.4rem] font-[700]">{'Wallet'}</h2>
            <div className="flex flex-col-reverse lg:flex-row lg:items-end gap-[8rem] mt-[4rem] sm:mt-[4.4rem]">
                <div className="lg:w-[55%]">
                   <div className="flex flex-col-reverse md:flex-row md:items-center gap-[3rem]">
                        <span className="text-white text-[1.8rem] font-[600]">{"© Wallet 2022"}</span>
                        <Link href={"#"}>
                            <span className="text-white text-[1.8rem] font-[600]">{"Privacy policy"}</span>
                        </Link>
                        <Link href={"#"}>
                            <span className="text-white text-[1.8rem] font-[600]">{"Cookies policy"}</span>
                        </Link>
                        <Link href={"#"}>
                            <span className="text-white text-[1.8rem] font-[600]">{"Terms of use"}</span>
                        </Link>
                   </div>
                </div>
                <div className="lg:w-[45%] flex lg:items-end lg:justify-end">
                    <form className="w-full">
                        <h4 className="text-white text-[1.8rem] font-[400]">{"Updates right to your Inbox"}</h4>
                        <div className="flex flex-col sm:flex-row sm:items-center gap-[2.5rem] mt-[1.5rem]">
                            <input type="email" placeholder="Email Address" className="h-[5rem] text-[1.8rem] w-full sm:w-[30.1rem] bg-dark_gray rounded-[1.5rem] px-[3rem] placeholder:text-[1.8rem] placeholder:font-[400] placeholder:text-white/40 focus:outline-none" />
                            <button type="submit" className="h-[5rem] px-[6.1rem] bg-purple rounded-[1.5rem] text-dark_gray">
                                <span className="text-[1.8rem]">{'Send'}</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}