"use client"

export default function Nav() {
    return (
       <div className="px-[2.5rem] sm:px-[5rem] py-[3.3rem]">
            <div className="flex items-center justify-between">
                <h1 className="text-[2.4rem] font-[700]">{'Wallet'}</h1>

                <div className="hidden sm:flex items-center gap-[3rem]">
                    <button type="button" className="h-[5rem] bg-transparent rounded-[1.5rem] text-white">
                        <span className="text-[1.8rem]">{'Sign up'}</span>
                    </button>
                    <button type="button" className="h-[5rem] px-[6.1rem] bg-purple rounded-[1.5rem] text-dark_gray">
                        <span className="text-[1.8rem]">{'Log in'}</span>
                    </button>
                </div>
            </div>
       </div>
    )
}