"use client"

import Image from "next/image"

export default function Features() {
    return(
        <div className="main-container py-[4rem] sm:py-[5rem]">
            <div className="px-[1rem] sm:px-[2.4rem]">
                <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-[5rem]">
                    <div className="text-center px-[6rem] sm:px-[4rem] lg:px-[6rem]">
                        <div className="relative h-[7rem] w-[7rem] mx-auto">
                            <Image src="/assets/icones/cards_icon.svg" fill alt="cards icon" />
                        </div>
                        <div className="mt-[4rem] mb-[1rem]">
                            <h4 className="text-[1.8rem] leading-[3.2rem] text-white font-[600]">{"Customizable card"}</h4>
                        </div>
                        <p className="text-[1.8rem] leading-[3.2rem] text-white/40 font-[400]">{"Custom your own card for your exact incomes and expenses needs."}</p>
                    </div>

                    <div className="text-center px-[6rem] sm:px-[4rem] lg:px-[6rem]">
                        <div className="relative h-[7rem] w-[7rem] mx-auto">
                            <Image src="/assets/icones/coin_icon.svg" fill alt="coin icon" />
                        </div>
                        <div className="mt-[4rem] mb-[1rem]">
                            <h4 className="text-[1.8rem] leading-[3.2rem] text-white font-[600]">{"No payment fee"}</h4>
                        </div>
                        <p className="text-[1.8rem] leading-[3.2rem] text-white/40 font-[400]">{"Transfer your payment all over the world with no payment fee."}</p>
                    </div>

                    <div className="text-center px-[5rem] sm:px-[3rem] lg:px-[5rem]">
                        <div className="relative h-[7rem] w-[7rem] mx-auto">
                            <Image src="/assets/icones/purse_icon.svg" fill alt="purse icon" />
                        </div>
                        <div className="mt-[4rem] mb-[1rem]">
                            <h4 className="text-[1.8rem] leading-[3.2rem] text-white font-[600]">{"All in one place"}</h4>
                        </div>
                        <p className="text-[1.8rem] leading-[3.2rem] text-white/40 font-[400]">{"The right place to keep your credit and debit cards, boarding passes & more."}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}