"use client"

import Image from "next/image"

export default function Cta() {
    return (
        <div className="main-container py-[7rem] md:py-[7.7rem]">
            <div className="block lg:flex items-center gap-[2rem]">
                <div className="w-full lg:w-[50%] text-center lg:text-left mb-[6rem] lg:mb-0">
                    <div className="max-w-[33.5rem] mx-auto lg:mx-0 lg:max-w-[46.5rem]">
                        <h2 className="text-[4rem] lg:text-[6rem] leading-[7rem] lg:leading-[9rem] font-[600]">{"Questions? Let’s talk "}</h2>
                    </div>
                    <div className="max-w-[33.5rem] mx-auto lg:mx-0 lg:max-w-[35rem] mt-[3rem] mb-[4rem]">
                        <p className="text-[1.8rem] leading-[3.2rem] font-[400] text-white/40">{"Contact us through our 24/7 live chat. We’re always happy to help!"}</p>
                    </div>
                    <button type="button" className="h-[5rem] px-[6.1rem] bg-yellow rounded-[1.5rem] text-dark_gray">
                        <span className="text-[1.8rem]">{'Get started'}</span>
                    </button>
                </div>

                <div className="lg:w-[50%] flex items-center justify-center">
                    <div className="relative h-[22.9rem] md:h-[44.6rem] w-[33.5rem] md:w-[50rem] rounded-[1.6rem] overflow-hidden">
                        <Image src="/assets/gif/folio.gif" fill alt="illustration" />
                    </div>
                </div>
            </div>
        </div>
    )
}