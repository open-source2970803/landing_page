"use client"

import Image from "next/image"

export default function Partner() {
    return (
        <div className="max-w-[1440px] mx-[20px] md:mx-[100px] border-y border-white/20 py-[5rem] sm:py-[6.8rem]">
            <div className="flex flex-wrap items-center justify-center gap-[4rem] sm:gap-[6rem] px-[6rem]">
                <div className="relative h-[3.5rem] w-[14.3rem]">
                    <Image src="/assets/logos/logo_1.png" fill alt="logo 1" />
                </div>
                <div className="relative h-[3.5rem] w-[4.2rem]">
                    <Image src="/assets/logos/logo_2.png" fill alt="logo 2" />
                </div>
                <div className="relative h-[3.5rem] w-[18.1rem]">
                    <Image src="/assets/logos/logo_3.png" fill alt="logo 3" />
                </div>
                <div className="relative h-[3.5rem] w-[12.3rem]">
                    <Image src="/assets/logos/logo_4.png" fill alt="logo 4" />
                </div>
                <div className="relative h-[3.5rem] w-[22.9rem]">
                    <Image src="/assets/logos/logo_5.png" fill alt="logo 5" />
                </div>
            </div>
        </div>
    )
}