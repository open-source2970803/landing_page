"use client"

import Image from "next/image"

export default function Feedback() {
    return (
        <div className="main-container py-[3rem]">
            <div className="flex flex-col justify-center h-auto lg:h-[46rem] bg-purple rounded-[2rem] px-[3rem] sm:px-[12rem] lg:px-[16.2rem] py-[8rem] lg:py-0">
                <p className="text-primary_gray text-[2.4rem] leading-[4rem] font-[600] text-center md:text-left">{"“Wallet is a great product! All of my most important information is there - credit cards, transit cards, boarding passes, tickets, and more. And I don't need to worry because it's all in one place! thanks!”"}</p>
                <div className="flex items-center justify-center lg:justify-start gap-[2rem] mt-[5rem]">
                    <div className="relative h-[8rem] w-[8rem] rounded-full overflow-hidden">
                        <Image src={"/assets/images/avatar.png"} fill alt="avatar" />
                    </div>
                    <div className="space-y-[0.5rem]">
                        <span className="text-primary_gray/40 text-[1.8rem] leading-[3.2rem] font-[400]">{"Johnny Owens"}</span>
                        <div className="flex items-center gap-[0.5rem]">
                            <div className="relative h-[2rem] w-[2rem]">
                                <Image src={"/assets/icones/star.svg"} fill alt="start" />
                            </div>
                            <div className="relative h-[2rem] w-[2rem]">
                                <Image src={"/assets/icones/star.svg"} fill alt="start" />
                            </div>
                            <div className="relative h-[2rem] w-[2rem]">
                                <Image src={"/assets/icones/star.svg"} fill alt="start" />
                            </div>
                            <div className="relative h-[2rem] w-[2rem]">
                                <Image src={"/assets/icones/star.svg"} fill alt="start" />
                            </div>
                            <div className="relative h-[2rem] w-[2rem]">
                                <Image src={"/assets/icones/star.svg"} fill alt="start" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}