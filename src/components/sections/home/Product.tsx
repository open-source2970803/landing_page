"use client"

import Image from "next/image"

export default function Product() {
    return (
        <div className="main-container py-[6rem] sm:py-[10rem]">
            <div className="relative bg-light_yellow h-[18rem] sm:h-[30rem] md:h-[40rem] lg:h-[50rem] w-full rounded-[1rem] md:rounded-[1.6rem]">
                <Image src="/assets/images/product.png" fill alt="illustration" />
            </div>
        </div>
    )
}