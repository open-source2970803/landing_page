import Footer from '@/components/layouts/Footer'
import Nav from '@/components/layouts/Nav'
import Cta from '@/components/sections/home/Cta'
import Features from '@/components/sections/home/Features'
import Feedback from '@/components/sections/home/Feedback'
import Header from '@/components/sections/home/Header'
import Partner from '@/components/sections/home/Partner'
import Product from '@/components/sections/home/Product'

export default function Home() {
  return (
    <>
      <Nav />
      <Header />
      <Partner />
      <Product />
      <Features />
      <Feedback />
      <Cta />
      <Footer />
    </>
  )
}
